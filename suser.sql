-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        8.0.23 - MySQL Community Server - GPL
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  10.2.0.5704
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 导出  表 example.suser 结构
CREATE TABLE IF NOT EXISTS `suser` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `uname` char(50) DEFAULT NULL COMMENT '用户账户（邮箱）',
  `pwd` char(50) DEFAULT NULL COMMENT '密码',
  `birthday` char(50) DEFAULT NULL COMMENT '生日',
  `type` char(50) DEFAULT NULL COMMENT '权限',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- 正在导出表  example.suser 的数据：~4 rows (大约)
/*!40000 ALTER TABLE `suser` DISABLE KEYS */;
INSERT INTO `suser` (`id`, `uname`, `pwd`, `birthday`, `type`) VALUES
	(1, '123456@qq.com', '123456', '2000-04-04', '系统管理员'),
	(2, '2191243294@qq.com', '123456', '2000-08-09', '实验室成员'),
	(4, '1622429851@qq.com', '123456', '2000-08-09', '普通用户'),
	(10, '123456789@qq.com', '1234567', '2000-04-04', '普通用户'),
	(11, '1146814223@qq.com', '123456789', '2000-04-04', '普通用户');
/*!40000 ALTER TABLE `suser` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
