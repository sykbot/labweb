-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        8.0.23 - MySQL Community Server - GPL
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  10.2.0.5704
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 导出  表 example.bloginfo 结构
CREATE TABLE IF NOT EXISTS `bloginfo` (
  `blogid` int NOT NULL AUTO_INCREMENT COMMENT '博客id',
  `userid` int NOT NULL COMMENT '用户id',
  `title` char(50) NOT NULL COMMENT '博客标题',
  `summary` char(50) NOT NULL COMMENT '博客概要',
  `content` longtext NOT NULL COMMENT '博客内容',
  `kind` char(50) NOT NULL COMMENT '博客类型',
  `pubdate` char(50) NOT NULL COMMENT '发布时间',
  PRIMARY KEY (`blogid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- 正在导出表  example.bloginfo 的数据：~2 rows (大约)
/*!40000 ALTER TABLE `bloginfo` DISABLE KEYS */;
INSERT INTO `bloginfo` (`blogid`, `userid`, `title`, `summary`, `content`, `kind`, `pubdate`) VALUES
	(1, 1, '如何开发一个实验室官网？', '本文将简述如何开发一个实验室官网', '此处省略50000字', 'php', '2021-08-13 14:52:11.764'),
	(3, 1, '如何学习java', '刻苦学习', '正文', 'java', '2021-08-13 20:19:42.107');
/*!40000 ALTER TABLE `bloginfo` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
