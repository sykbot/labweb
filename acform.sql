-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        8.0.23 - MySQL Community Server - GPL
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  10.2.0.5704
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 导出  表 example.acform 结构
CREATE TABLE IF NOT EXISTS `acform` (
  `acid` int DEFAULT NULL COMMENT '活动id',
  `userid` int DEFAULT NULL COMMENT '用户id',
  `email` char(50) DEFAULT NULL COMMENT '电子邮箱',
  `perintro` varchar(50) DEFAULT NULL COMMENT '个人介绍',
  KEY `FK_acform_activity` (`acid`),
  CONSTRAINT `FK_acform_activity` FOREIGN KEY (`acid`) REFERENCES `activity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- 正在导出表  example.acform 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `acform` DISABLE KEYS */;
/*!40000 ALTER TABLE `acform` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
