package com.syk.labweb.Mapper;


import com.syk.labweb.enitity.Bloginfo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BloginfoMapper {
    void chagebloginfo(Bloginfo bloginfo);

    void deletebloginfo(int blogid);

    void insertbloginfo(Bloginfo bloginfo);

    String getTitle(int blogid);

    Bloginfo readblog(int blogid);
}
