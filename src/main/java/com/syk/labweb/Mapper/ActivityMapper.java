package com.syk.labweb.Mapper;


import com.syk.labweb.enitity.Acform;
import com.syk.labweb.enitity.Activity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ActivityMapper {
    void insertac(Activity activity);

    List<Activity> showac();

    Acform issign(@Param(value = "acid")int acid, @Param(value = "userid") int userid);

    void signup(Acform acform);

    void unsignup(@Param(value = "acid")int acid, @Param(value = "userid") int userid);

    void numdown(@Param(value = "id")int id);

    void numup(@Param(value = "id")int id);

}
