package com.syk.labweb.Mapper;

import com.syk.labweb.enitity.Collecte;
import com.syk.labweb.enitity.Like;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface BlogOpMapper {

    void like(Like like);

    void unlike(Like like);

    Like findLike(Like like);




    void collect(@Param("collectionid") int collectionid,@Param("blogid") int blogid);

    void uncollect(@Param("collectionid") int collectionid,@Param("blogid") int blogid);

    Collecte havecollect(@Param("collectionid") int collectionid, @Param("blogid") int blogid);




    void comment(@Param("commentid") int commentid,@Param("blogid") int blogid);

    void uncomment(@Param("commentid") int commentid,@Param("blogid") int blogid);

}
