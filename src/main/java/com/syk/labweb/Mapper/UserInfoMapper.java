package com.syk.labweb.Mapper;


import com.syk.labweb.enitity.Userinfo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface UserInfoMapper {

    void addbaseinfo();

    Userinfo findUserinfo(int id);

    void chageinfo(Userinfo userinfo);
}
