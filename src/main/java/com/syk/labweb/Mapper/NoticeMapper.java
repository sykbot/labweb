package com.syk.labweb.Mapper;


import com.syk.labweb.enitity.Notice;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface NoticeMapper {

    void insertnt(Notice notice);

    List<Notice> show();

}
