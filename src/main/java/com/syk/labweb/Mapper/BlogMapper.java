package com.syk.labweb.Mapper;


import com.syk.labweb.enitity.Blog;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface BlogMapper {
    Blog selectblog(int blogid);

    List<Blog> selectlater();

    List<Integer> finduserid();

    void addLike(int blogid);

    void deleteLike(int blogid);

    void insertblog(Blog blog);

    void addCollection(int blogid);

    void deleteCollection(int blogid);
}
