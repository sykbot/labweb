package com.syk.labweb.Mapper;

import com.syk.labweb.enitity.Comment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface CommentMapper {

    List<Comment> selectAll(int blogid);

    void commentblog(Comment comment);

    void commentcm(Comment comment);

    void deletecomment(int commentid);

    List<Comment> selectsdcm( @Param("blogid") int blogid,@Param("commentid") int commentid);

    List<Comment> selectacAll(int acid);

    void commentac(Comment comment);

    void commentaccm(Comment comment);

    List<Comment> selectacsdcm(@Param("acid") int acid, @Param("commentid") int commentid);
}
