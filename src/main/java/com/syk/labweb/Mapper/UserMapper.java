package com.syk.labweb.Mapper;


import com.syk.labweb.enitity.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface UserMapper {
   User login(User user);

   void register(User user);

   User findUser(String uname);

   void chagepwd(User user);

   void chageroot(User user);

   String returnroot(int userid);


}
