package com.syk.labweb.Mapper;

import com.syk.labweb.enitity.BlogMsg;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface BlogMsgMapper {

    List<BlogMsg> findMsg(int userid);

    void insertLike(BlogMsg blogMsg);

    void insertCollection(BlogMsg blogMsg);

    void insertComment(BlogMsg blogMsg);

    void haveread(int msgid);

    void haveallread();

    void deleteMsg(int msgid);

}
