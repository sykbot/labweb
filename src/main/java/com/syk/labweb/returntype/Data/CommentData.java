package com.syk.labweb.returntype.Data;

import com.syk.labweb.enitity.Comment;
import lombok.Data;

import java.util.List;

/**
 * 分页获取评论的状态返回
 */
@Data
public class CommentData {

    //分页序号
    private int pageNo;

    //总条数
    private long total;

    //返回具体数据
    private Object data;

    public CommentData(int pageNo, long total, Object data){
        this.pageNo = pageNo;
        this.total = total;
        this.data = data;
    }
}
