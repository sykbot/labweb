package com.syk.labweb.returntype.Data;

import com.syk.labweb.enitity.BlogMsg;
import com.syk.labweb.enitity.Userinfo;
import lombok.Data;

@Data
public class MsgData {
    /**
     * 点赞消息id
     */
    private int msgid;
    /**
     * url
     */
    private String url;
    /**
     * 用户信息
     */
    private Userinfo userinfo;
    /**
     * 博客id
     */
    private int blogid;
    /**
     * 博客标题
     */
    private String blogName;
    /**
     * 消息内容
     */
    private String content;
    /**
     * 是否已读
     */
    private int isRead;



    public MsgData(BlogMsg blogMsg, Userinfo userinfo) {
        this.msgid = blogMsg.getMsgid();
        this.url = blogMsg.getUrl();
        this.userinfo = userinfo;
        this.blogid = blogMsg.getBlogid();
        this.blogName = blogMsg.getBlogName();
        this.content=blogMsg.getContent();
        this.isRead = blogMsg.getIsRead();
    }
}
