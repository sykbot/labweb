package com.syk.labweb.returntype.Data;


import com.syk.labweb.enitity.Blog;
import com.syk.labweb.enitity.Userinfo;
import lombok.Data;

/**
 * 博客相关信息数据状态返回
 */
@Data
public class BlogData {
    /**
     * 用户信息
     */
    private Userinfo userinfo;
    /**
     * 博文id
     */
    private int blogid;
    /**
     * 点赞数
     */
    private int like;
    /**
     * 收藏数
     */
    private int collect;
    /**
     * 浏览量
     */
    private String views;
    /**
     * 发布时间
     */
    private String pubdate;
    /**
     * 博文题目
     */
    private String title;
    /**
     * 博文摘要
     */
    private String summary;

    public BlogData(Userinfo userinfo, Blog blog) {
        this.userinfo = userinfo;
        this.blogid = blog.getUserid();
        this.like = blog.getLikenum();
        this.collect = blog.getCollect();
        this.views = blog.getViews();
        this.pubdate = blog.getPubdate();
        this.title = blog.getTitle();
        this.summary = blog.getSummary();
    }
}
