package com.syk.labweb.returntype;

import lombok.Data;

/**
 * 定义一个通用的返回类型
 * @author syk
 */
@Data
public class Result {

    //成功标志
    private boolean success;

    //返回处理消息
    private String message;

    //返回状态码
    private int code;

    //返回具体数据（如用户账户信息）
    /** 这里result返回Object类型 可以将Data包里的数据传过来 */
    private Object result;

    public Result(){}

    public Result(boolean success,String message,int code){
        this.success=success;
        this.message=message;
        this.code=code;

    }

    public Result(boolean success,String message,int code,Object result){
        this.success=success;
        this.message=message;
        this.code=code;
        this.result=result;
    }

}
