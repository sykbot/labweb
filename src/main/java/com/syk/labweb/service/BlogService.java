package com.syk.labweb.service;

import com.syk.labweb.enitity.Blog;

import java.util.List;

public interface BlogService {

    Blog selectblog(int blogid);

    List<Blog> selectlater(int pageNo, int pageSize);

}
