package com.syk.labweb.service;

import com.syk.labweb.enitity.Acform;
import com.syk.labweb.enitity.Activity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ActivityService {
    void insertac(Activity activity);

    List<Activity> showac();

    int issign(int acid,int userid);

    void signup(Acform acform);

    void unsignup(@Param(value = "acid")int acid, @Param(value = "userid") int userid);
}
