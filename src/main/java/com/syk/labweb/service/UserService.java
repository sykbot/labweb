package com.syk.labweb.service;


import com.syk.labweb.enitity.User;
import com.syk.labweb.enitity.Userinfo;

import java.util.List;

public interface UserService{
    User login(User user);

    void register(User user);

    void addbaseinfo();

    User findUser(String uname);

    Userinfo findUserinfo(int id);

    void chageinfo(Userinfo userinfo);

    int chagepwd(User user);

    void chageroot(User user);

    String returnroot(int userid);
}
