package com.syk.labweb.service.impl;


import com.github.pagehelper.PageHelper;
import com.syk.labweb.Mapper.BlogMapper;
import com.syk.labweb.enitity.Blog;
import com.syk.labweb.enitity.Comment;
import com.syk.labweb.service.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BlogServiceimpl implements BlogService {

    @Autowired
    private BlogMapper blogDao;

    @Override
    public Blog selectblog(int blogid) {
        return blogDao.selectblog(blogid);
    }
    @Override
    public List<Blog> selectlater(int pageNo, int pageSize) {
        PageHelper.startPage(pageNo, pageSize);
        List<Blog> list=blogDao.selectlater();
        return list;
    }


}
