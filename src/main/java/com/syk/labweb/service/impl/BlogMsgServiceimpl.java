package com.syk.labweb.service.impl;


import com.syk.labweb.Mapper.BlogMsgMapper;
import com.syk.labweb.enitity.BlogMsg;
import com.syk.labweb.service.BlogMsgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BlogMsgServiceimpl implements BlogMsgService {

    @Autowired
    private BlogMsgMapper BlogMsgDao;

    @Override
    public List<BlogMsg> findMsg(int userid) {
        return BlogMsgDao.findMsg(userid);
    }

    @Override
    public void insertLike(BlogMsg blogMsg) {
        BlogMsgDao.insertLike(blogMsg);
    }

    @Override
    public void insertCollection(BlogMsg blogMsg) {BlogMsgDao.insertCollection(blogMsg);}

    @Override
    public void insertComment(BlogMsg blogMsg) { BlogMsgDao.insertComment(blogMsg);}

    @Override
    public void haveread(int msgid) {
        BlogMsgDao.haveread(msgid);
    }

    @Override
    public void haveallread() {
        BlogMsgDao.haveallread();
    }

    @Override
    public void deleteMsg(int msgid) {
        BlogMsgDao.deleteMsg(msgid);
    }
}
