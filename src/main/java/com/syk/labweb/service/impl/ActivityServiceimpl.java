package com.syk.labweb.service.impl;

import com.syk.labweb.Mapper.ActivityMapper;
import com.syk.labweb.enitity.Acform;
import com.syk.labweb.enitity.Activity;
import com.syk.labweb.service.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActivityServiceimpl implements ActivityService {
    @Autowired
    private ActivityMapper activityDao;


    @Override
    public void insertac(Activity activity) {
     activityDao.insertac(activity);
    }

    @Override
    public List<Activity> showac() {
        return activityDao.showac();
    }

    @Override
    public int issign(int acid, int userid) {
        //判别用户是否已经报过名是返回0没有返回1
       if(activityDao.issign(acid, userid)!=null){
           return 0;
       }
        return 1;
    }

    @Override
    public void signup(Acform acform) {
    activityDao.signup(acform);
    activityDao.numdown(acform.getAcid());
    }

    @Override
    public void unsignup(int acid, int userid) {
    activityDao.unsignup(acid, userid);
    activityDao.numup(acid);
    }
}
