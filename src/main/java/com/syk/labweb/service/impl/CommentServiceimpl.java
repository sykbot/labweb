package com.syk.labweb.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.syk.labweb.Mapper.BlogOpMapper;
import com.syk.labweb.Mapper.CommentMapper;
import com.syk.labweb.enitity.Comment;
import com.syk.labweb.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentServiceimpl implements CommentService {

   @Autowired
   private  CommentMapper commentDao;

   @Autowired
   private BlogOpMapper blogOpDao;

    /**
     * 获取分页数据
     * @param pageNo
     * @param pageSize
     * @return
     */
    @Override
    public  List<Comment> selectAll(int blogid,int pageNo, int pageSize) {
        PageHelper.startPage(pageNo, pageSize);
        List<Comment> list=commentDao.selectAll(blogid);
        for (Comment a:list) {
            a.addcomment(selectsdcm(a.getBlogid(),a.getCommentid()));
        }
        return list;
    }

    /**
     * 获取总页数
     * @param blogid
     * @return
     */
    @Override
    public long gettotal(int blogid){
        List<Comment> list=commentDao.selectAll(blogid);
        PageInfo<Comment> pageinfo=new PageInfo<>(list);
        return pageinfo.getTotal();
    }

    /**
     * 评论博客
     * @param comment
     */
    @Override
    public void commentblog(Comment comment) {
        blogOpDao.comment(comment.getCommentid(),comment.getBlogid());
        commentDao.commentblog(comment);
    }

    /**
     * 评论博客的评论
     * @param comment
     */
    @Override
    public void commentcm(Comment comment) {
        blogOpDao.comment(comment.getCommentid(),comment.getBlogid());
        commentDao.commentcm(comment);
    }

    /**
     *删除评论
     * @param commentid
     * @param blogid
     */
    @Override
    public void uncomment(int commentid, int blogid) {
        blogOpDao.uncomment(commentid, blogid);
    }

    @Override
    public void deletecomment(int commentid) {
        commentDao.deletecomment(commentid);
    }

    /**
     * 返回博客下级评论
     * @param blogid
     * @param userid
     * @return
     */
    @Override
    public List<Comment> selectsdcm(int blogid, int userid) {
        return commentDao.selectsdcm(blogid, userid);
    }

    /**
     * 返回活动评论
     * @param acid
     * @param pageNo
     * @param pageSize
     * @return
     */
    @Override
    public List<Comment> selectacAll(int acid,int pageNo, int pageSize) {
        PageHelper.startPage(pageNo, pageSize);
        List<Comment> list=commentDao.selectacAll(acid);
        for (Comment a:list) {
            a.addcomment(selectacsdcm(a.getAcid(),a.getCommentid()));
        }
        return list;
    }

    /**
     * 获取活动评论总页数
     * @param acid
     * @return
     */
    @Override
    public long getactotal(int acid){
        List<Comment> list=commentDao.selectacAll(acid);
        PageInfo<Comment> pageinfo=new PageInfo<>(list);
        return pageinfo.getTotal();
    }

    /**
     * 评论活动
     * @param comment
     */
    @Override
    public void commentac(Comment comment) {
        commentDao.commentac(comment);
    }

    /**
     * 评论活动评论
     * @param comment
     */
    @Override
    public void commentaccm(Comment comment) {
        commentDao.commentaccm(comment);
    }

    /**
     * 返回博客下级评论
     * @param acid
     * @param userid
     * @return
     */
    @Override
    public List<Comment> selectacsdcm(int acid, int userid) {
        return commentDao.selectacsdcm(acid, userid);
    }

}
