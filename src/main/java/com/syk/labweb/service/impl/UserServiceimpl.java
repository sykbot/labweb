package com.syk.labweb.service.impl;

import com.syk.labweb.Mapper.UserInfoMapper;
import com.syk.labweb.Mapper.UserMapper;
import com.syk.labweb.enitity.User;
import com.syk.labweb.enitity.Userinfo;
import com.syk.labweb.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceimpl implements UserService {
    @Autowired
    private UserMapper userDao;

    @Autowired
    private UserInfoMapper userInfoDao;

    /**登录 */
    @Override
    public User login(User user){
        return userDao.login(user);
    }
    /**注册账户和密码 */
    @Override
    public void register(User user){
        userDao.register(user);
    }
    /**注册后自动填写用户的基本信息*/
    @Override
    public void addbaseinfo(){ userInfoDao.addbaseinfo();}
    /**查找是否有该用户 */
    @Override
    public User findUser(String uname){
        return userDao.findUser(uname);
    }
    /**查找该id下的用户基本信息*/
    @Override
    public Userinfo findUserinfo(int id){return userInfoDao.findUserinfo(id);}

    @Override
    public void chageinfo(Userinfo userinfo) { userInfoDao.chageinfo(userinfo);}

    @Override
    public int chagepwd(User user) {
        //密码不能小于6位,小于返回0,反之返回1
        if(user.getPwd().length()<=6){
            return 0;
        }
        userDao.chagepwd(user);
        return 1;
    }

    @Override
    public void chageroot(User user) {
        userDao.chageroot(user);
    }

    @Override
    public String returnroot(int userid) {
        return userDao.returnroot(userid);
    }


}
