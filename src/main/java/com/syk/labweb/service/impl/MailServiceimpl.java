package com.syk.labweb.service.impl;

import com.syk.labweb.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class MailServiceimpl implements MailService {

    @Autowired
    private JavaMailSender Sender;

    //邮件发送人
    @Value("${spring.mail.username}")
    private String from;

    @Override
    public void sendMail(String getter, String code) {
        SimpleMailMessage message = new SimpleMailMessage();

        //发送人
        message.setFrom(from);

        //收信人
        message.setTo(getter);

        //主题
        message.setSubject("SYK系统博客");

        //正文
        message.setText("验证码："+code+"     请勿告诉他人！");

        //发送
        try {
            Sender.send(message);
        }
        catch (Exception e){
            e.toString();
        }



    }
}
