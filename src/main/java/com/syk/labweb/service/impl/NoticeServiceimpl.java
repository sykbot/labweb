package com.syk.labweb.service.impl;


import com.syk.labweb.Mapper.NoticeMapper;
import com.syk.labweb.enitity.Notice;
import com.syk.labweb.service.NoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NoticeServiceimpl implements NoticeService {

    @Autowired
    private NoticeMapper noticeDao;

    @Override
    public void insertnt(Notice notice) {
        noticeDao.insertnt(notice);
    }

    @Override
    public List<Notice> show() {
        return noticeDao.show();
    }
}
