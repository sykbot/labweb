package com.syk.labweb.service.impl;


import com.syk.labweb.Mapper.BlogMapper;
import com.syk.labweb.Mapper.BlogOpMapper;
import com.syk.labweb.enitity.Collecte;
import com.syk.labweb.enitity.Like;
import com.syk.labweb.service.BlogOpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BlogOpServiceimpl implements BlogOpService {

    @Autowired
    private BlogOpMapper blogOpDao;

    @Autowired
    private BlogMapper blogDao;

    /**
     * 点赞和取消点赞操作
     * @param like
     */
    @Override
    public int islike(Like like) {
        int blogid=like.getBlogid();
        //判别之前对同一个用户是否已经点过赞，若无则进行点赞操作，若有则是取消点赞
        if(blogOpDao.findLike(like) !=null){
            //进行取消点赞操作，因为该用户对此博客已经点赞
            blogOpDao.unlike(like);
            blogDao.deleteLike(blogid);
            return 0;
        }
        else{
            //若此先没有进行点赞，则进行点赞操作
            blogOpDao.like(like);
            blogDao.addLike(blogid);
            return 1;
        }
    }

    @Override
    public int iscollect(int collectionid, int blogid) {
        //判别之前对同一个用户是否已经点过赞，若无则进行点赞操作，若有则是取消点赞
        if(blogOpDao.havecollect(collectionid, blogid) !=null){
            //进行取消点赞操作，因为该用户对此博客已经点赞
            blogOpDao.uncollect(collectionid, blogid);
            blogDao.deleteCollection(blogid);
            return 0;
        }
        else{
            //若此先没有进行点赞，则进行点赞操作
            blogOpDao.collect(collectionid, blogid);
            blogDao.addCollection(blogid);
            return 1;
        }
    }



}
