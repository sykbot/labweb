package com.syk.labweb.service.impl;


import com.syk.labweb.Mapper.BlogMapper;
import com.syk.labweb.Mapper.BloginfoMapper;
import com.syk.labweb.enitity.Blog;
import com.syk.labweb.enitity.Bloginfo;
import com.syk.labweb.service.BloginfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BloginfoServiceimpl implements BloginfoService {
    @Autowired
    private BloginfoMapper bloginfoDao;

    @Autowired
    private BlogMapper blogDao;

    @Override
    public void chagebloginfo(Bloginfo bloginfo) {
        bloginfoDao.chagebloginfo(bloginfo);
    }

    @Override
    public void deletebloginfo(int blogid) { bloginfoDao.deletebloginfo(blogid); }

    @Override
    public void insertbloginfo(Bloginfo bloginfo) {
        bloginfoDao.insertbloginfo(bloginfo);
        blogDao.insertblog(new Blog(bloginfo.getUserid(),bloginfo.getTitle(),bloginfo.getSummary(),bloginfo.getPubdate()));
    }

    @Override
    public String getTitle(int blogid) {
        return bloginfoDao.getTitle(blogid);
    }

    @Override
    public Bloginfo readblog(int blogid) {
        return bloginfoDao.readblog(blogid);
    }
}
