package com.syk.labweb.service;

import com.syk.labweb.enitity.Bloginfo;

public interface BloginfoService {
    void chagebloginfo(Bloginfo bloginfo);

    void deletebloginfo(int blogid);

    void insertbloginfo(Bloginfo bloginfo);

    String getTitle(int blogid);

    Bloginfo readblog(int blogid);
}
