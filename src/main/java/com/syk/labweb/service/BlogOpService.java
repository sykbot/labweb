package com.syk.labweb.service;

import com.syk.labweb.enitity.Collecte;
import com.syk.labweb.enitity.Like;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BlogOpService {

    int islike(Like like);

    int iscollect(int collectionid,int blogid);


}
