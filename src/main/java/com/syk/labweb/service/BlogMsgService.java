package com.syk.labweb.service;

import com.syk.labweb.enitity.BlogMsg;

import java.util.List;

public interface BlogMsgService {

    List<BlogMsg> findMsg(int userid);

    void insertLike(BlogMsg blogMsg);

    void insertCollection(BlogMsg blogMsg);

    void insertComment(BlogMsg blogMsg);

    void haveread(int msgid);

    void haveallread();

    void deleteMsg(int msgid);


}
