package com.syk.labweb.service;

import com.syk.labweb.enitity.Comment;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CommentService {

    List<Comment> selectAll(int blogid,int pageNo, int pageSize);

    long gettotal(int blogid);

    void commentblog(Comment comment);

    void commentcm(Comment comment);

    void uncomment(int commentid,int blogid);

    void deletecomment(int commentid);

    List<Comment> selectsdcm(int blogid,int commentid);

    List<Comment> selectacAll(int acid,int pageNo, int pageSize);

    long getactotal(int acid);

    void commentac(Comment comment);

    void commentaccm(Comment comment);

    List<Comment> selectacsdcm(int acid, int userid);
}
