package com.syk.labweb.controller;


import com.syk.labweb.enitity.Acform;
import com.syk.labweb.enitity.Activity;
import com.syk.labweb.returntype.Result;
import com.syk.labweb.service.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class ActivityController {
    @Autowired
    private ActivityService activityService;

    /**
     * 展示正在开展的活动
     * @return
     */
    @GetMapping("/activity/show")
    public Result show(){
        List<Activity> list=activityService.showac();
        return new Result(true,"展示活动",200,list);
    }

    /**
     * 发布活动
     */
    @PostMapping("/activity/release")
    public Result release(@RequestParam Map<String,String> map){
        Activity activity=new Activity(map);
        activityService.insertac(activity);
        return new Result(true,"发布成功",200,activity);
    }

    /**
     * 报名/取消报名接口
     * @param map
     * @return
     */
    @PostMapping("/activity/sign")
    public Result issign(@RequestParam Map<String,String> map){
        int acid= Integer.parseInt(map.get("acid"));
        int userid= Integer.parseInt(map.get("userid"));
        int bl=activityService.issign(acid,userid);
        if(bl==1){
            Acform acform=new Acform(map);
            activityService.signup(acform);
            return new Result(true,"报名成功！！！",200,acform);
        }
        else {
            activityService.unsignup(acid,userid);
            return new Result(true,"取消报名成功！！！",200,null);
        }
    }
}
