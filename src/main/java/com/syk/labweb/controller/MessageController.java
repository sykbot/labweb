package com.syk.labweb.controller;

import com.syk.labweb.enitity.BlogMsg;
import com.syk.labweb.returntype.Data.MsgData;
import com.syk.labweb.returntype.Result;
import com.syk.labweb.service.BlogMsgService;
import com.syk.labweb.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class MessageController {

    @Autowired
    private BlogMsgService blogMsgService;

    @Autowired
    private UserService userService;
    /**
     * 获取我的消息
     * @param userid
     * @return
     */
    @GetMapping("/message/like/myMsg")
    public Result selectMyMsg(@RequestParam(value = "userid") int userid){
        List<BlogMsg> listMsg= blogMsgService.findMsg(userid);
        List<MsgData> list=new ArrayList();
        for (BlogMsg a:listMsg) {
            list.add(new MsgData(a,userService.findUserinfo(a.getUserid())));
        }
        return new Result(true,"获取成功",200,list);
    }

    /**
     * 已读消息
     * @param msgid
     * @return
     */
    @PutMapping("/message/like/readMsg")
    public Result readlikeMsg(@RequestParam(value = "msgid") int msgid){
        if(msgid!=0){
            blogMsgService.haveread(msgid);
            return new Result(true,"已读该条信息",200,null);
        }
        else{
            blogMsgService.haveallread();
            return new Result(true,"已读所有信息",200,null);
        }
    }

    /**
     * 删除消息
     * @param msgid
     * @return
     */
    @DeleteMapping("/message/like/deleteMsg")
    public Result deleteMsg(@RequestParam(value = "msgid") int msgid){
        blogMsgService.deleteMsg(msgid);
        return new Result(true,"已删除该信息",200,null);
    }
}
