package com.syk.labweb.controller;


import com.syk.labweb.enitity.BlogMsg;
import com.syk.labweb.enitity.Comment;
import com.syk.labweb.returntype.Data.CommentData;
import com.syk.labweb.returntype.Result;
import com.syk.labweb.service.BlogMsgService;
import com.syk.labweb.service.BloginfoService;
import com.syk.labweb.service.CommentService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * 评论分页获取信息控制器
 * @author syk
 */

@RestController
public class CommentController {

    @Autowired
    private CommentService commentService;

    @Autowired
    private BlogMsgService blogMsgService;

    @Autowired
    private BloginfoService bloginfoService;

    /**
     * 分页获取博客评论接口
     * @param blogid
     * @param pageNo
     * @param pageSize
     * @return
     */
    @GetMapping("/comments/blog/show")
    public Result commentgetter(@RequestParam(value = "blogid") int blogid,
                                @RequestParam(value = "pageNo",defaultValue = "0") int pageNo,
                                @RequestParam(value = "pageSize",defaultValue = "5") int pageSize){
        List list=commentService.selectAll(blogid,pageNo,pageSize);
        long total=commentService.gettotal(blogid);
        //编辑评论的具体数据（第几页，总条数）
        CommentData commentData =new CommentData(pageNo,total,list);
        return  new Result(true,"操作成功",200, commentData);
    }

    /**
     * 评论博客接口
     * @param map
     * @return
     */
    @PostMapping("/comments/blog/commentblog")
    public Result commentblog(HttpServletRequest request,
                              @RequestParam Map<String,String> map){
        String url=request.getRequestURI();
        Comment comment=new Comment(map);
        commentService.commentblog(comment);
        //发送评论消息给博主
        blogMsgService.insertComment(new BlogMsg(url,Integer.parseInt(map.get("userid")),Integer.parseInt(map.get("blogid")),bloginfoService.getTitle(Integer.parseInt(map.get("blogid")))));
        return  new Result(true,"评论博客成功",200, comment);
    }

    /**
     * 评论博客的评论接口
     * @param request
     * @param map
     * @return
     */
    @PostMapping("/comments/blog/commentcm")
    public Result commentcm(HttpServletRequest request,
                            @RequestParam Map<String,String> map){
        String url=request.getRequestURI();
        Comment comment=new Comment(map);
        //上级评论id
        comment.setCommentid(Integer.parseInt(map.get("commentid")));
        commentService.commentcm(comment);
        //发送评论消息给上级评论的人
        blogMsgService.insertComment(new BlogMsg(url,Integer.parseInt(map.get("userid")),Integer.parseInt(map.get("blogid")),bloginfoService.getTitle(Integer.parseInt(map.get("blogid")))));
        return  new Result(true,"评论博客的评论成功",200, comment);
    }


    /**
     * 删除博客评论接口
     * @param commentid
     * @param blogid
     * @return
     */
    @DeleteMapping("/comments/blog/delete")
    public Result uncomment(@RequestParam(value = "commentid") int commentid,
                            @RequestParam(value = "blogid") int blogid){
        commentService.uncomment(commentid, blogid);
        commentService.deletecomment(commentid);
        return  new Result(true,"删除评论成功",200,null);
    }

    /**
     * 分页获取活动评论接口
     * @param acid
     * @param pageNo
     * @param pageSize
     * @return
     */
    @GetMapping("/comments/activity/show")
    public Result commentacgetter(@RequestParam(value = "acid") int acid,
                                @RequestParam(value = "pageNo",defaultValue = "0") int pageNo,
                                @RequestParam(value = "pageSize",defaultValue = "5") int pageSize){
        List list=commentService.selectacAll(acid,pageNo,pageSize);
        long total=commentService.getactotal(acid);
        //编辑评论的具体数据（第几页，总条数）
        CommentData commentData =new CommentData(pageNo,total,list);
        return  new Result(true,"操作成功",200, commentData);
    }

    /**
     * 评论活动接口
     * @param map
     * @return
     */
    @PostMapping("/comments/activity/commentac")
    public Result commentac(HttpServletRequest request,
                              @RequestParam Map<String,String> map){
        String url=request.getRequestURI();
        Comment comment=new Comment(map);
        commentService.commentac(comment);
        return  new Result(true,"评论活动成功",200, comment);
    }

    /**
     * 评论活动的评论接口
     * @param request
     * @param map
     * @return
     */
    @PostMapping("/comments/activity/commentcm")
    public Result commentaccm(HttpServletRequest request,
                            @RequestParam Map<String,String> map){
        String url=request.getRequestURI();
        Comment comment=new Comment(map);
        //上级评论id
        comment.setCommentid(Integer.parseInt(map.get("commentid")));
        System.out.println(comment);
        commentService.commentaccm(comment);
        return  new Result(true,"评论活动的评论成功",200, comment);
    }


    /**
     * 删除博客评论接口
     * @param  commentid
     * @return
     */
    @DeleteMapping("/comments/activity/delete")
    public Result unaccomment(@RequestParam(value = "commentid") int commentid){
        commentService.deletecomment(commentid);
        return  new Result(true,"删除活动评论成功",200,null);
    }




}
