package com.syk.labweb.controller;


import com.syk.labweb.enitity.Blog;
import com.syk.labweb.enitity.Bloginfo;
import com.syk.labweb.enitity.Userinfo;
import com.syk.labweb.returntype.Data.BlogData;
import com.syk.labweb.returntype.Result;
import com.syk.labweb.service.BlogService;
import com.syk.labweb.service.BloginfoService;
import com.syk.labweb.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
public class BlogController {

    @Autowired
    private BlogService blogService;

    @Autowired
    private BloginfoService bloginfoService;

    @Autowired
    private UserService userService;

    /**
     * 修改博客
     * @param map
     * @return
     */
    @PutMapping("/blog/change")
    public Result chageblog(@RequestParam Map<String,String> map){
        Bloginfo bloginfo=new Bloginfo(map);
        bloginfoService.chagebloginfo(bloginfo);
        return new Result(true,"修改博客成功",200,null);

    }

    /**
     * 删除博客接口
     * @param blogid
     * @return
     */
    @DeleteMapping("/blog/delete")
    public Result deletebloginfo(@RequestParam(value = "blogid") int blogid){
        bloginfoService.deletebloginfo(blogid);
        return new Result(true,"删除博客成功",200,null);
    }


    /**
     * 发布博客接口
     * @param map
     * @return
     */
    @PostMapping("/blog/release")
    public Result release(@RequestParam Map<String,String> map){
        bloginfoService.insertbloginfo(new Bloginfo(map));
        return new Result(true,"发布博客成功",200,null);
    }
    /**
     * 根据id选择博客接口
     * @param blogid
     * @return
     */
    @GetMapping("/blog/selectblog")
    public Result selectblog(@RequestParam(value = "blogid") int blogid){
        Blog blog= blogService.selectblog(blogid);
        Userinfo  userinfo=userService.findUserinfo(blog.getUserid());
        BlogData blogData=new BlogData(userinfo,blog);
        return new Result(true,"quis",51,blogData);
    }

    /**
     * 分页获取最近的博客接口
     * @param pageNo
     * @param pageSize
     * @return
     */
    @GetMapping("/blog/list")
    public Result list(@RequestParam(value = "pageNo",defaultValue = "0") int pageNo,
                       @RequestParam(value = "pageSize",defaultValue = "5") int pageSize){
        List<Blog> listblog=blogService.selectlater(pageNo, pageSize);
        List<BlogData> list = new ArrayList();
        for (Blog a:listblog) {
            list.add(new BlogData(userService.findUserinfo(a.getUserid()),a));
        }
        return new Result(true,"获取成功",200,list);
    }


    /**
     * 读取博文详情查看接口
     * @param blogid
     * @return
     */
    @GetMapping("/blog/read")
    public Result readblog(@RequestParam(value = "blogid") int blogid){
        Bloginfo bloginfo= bloginfoService.readblog(blogid);
        return new Result(true,"读取博文详情成功",400,bloginfo.toString());
    }
}
