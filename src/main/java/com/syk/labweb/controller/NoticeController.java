package com.syk.labweb.controller;


import com.syk.labweb.enitity.Notice;
import com.syk.labweb.returntype.Result;
import com.syk.labweb.service.NoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class NoticeController {
    @Autowired
    private NoticeService noticeService;


    @GetMapping("/notice/show")
    public Result show(){
        List<Notice> list=noticeService.show();
        return new Result(true,"显示公告",200,list);
    }


    @PostMapping("/notice/release")
    public Result release(@RequestParam Map<String,String> map){
        Notice notice=new Notice(map);
        noticeService.insertnt(notice);
        return new Result(true,"发布公告",200,notice);
    }



}
