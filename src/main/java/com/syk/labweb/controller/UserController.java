package com.syk.labweb.controller;


import com.syk.labweb.enitity.User;
import com.syk.labweb.enitity.Userinfo;
import com.syk.labweb.returntype.Result;
import com.syk.labweb.security.VerificationCode;
import com.syk.labweb.service.MailService;
import com.syk.labweb.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 用户登录注册控制器
 * @author syk
 */
@RestController
public class UserController {
    @Autowired
    private UserService userservice;

    @Autowired
    private MailService mailService;


    private VerificationCode verificationCode;
    /**
     * 登录接口
     */
    @PostMapping("/user/login")
    public Result login(@RequestParam Map<String,String> map){
        User user=new User(map.get("uname"),map.get("pwd"),map.get("type"));
        System.out.println(userservice.login(user));
        if (userservice.login(user) != null){return new Result(true,"用户登录成功",200,user);}
        else{ return new Result(false,"用户登录失败，账号或密码错误！！！",304,null);}
    }

    /**
     * 注册接口（刚注册时是普通用户）
     * @param map
     * @return
     */
    @PostMapping("/user/register")
    public Result register(@RequestParam Map<String,String> map){

        String uname=map.get("uname");
        String pwd=map.get("pwd");
        String birthday=map.get("birthday");
        String code=map.get("code");
        User user=new User(uname,pwd,birthday,"普通用户");
        //发送验证码时已经判别是否有重复的用户名，所以这里不需要判断是否有重复用户名
        //判别验证码是否正确
        if(verificationCode.selectCode(uname).equals(code)){
            //判断验证码是否过期
            if(verificationCode.isvalid(uname,System.currentTimeMillis())){
                userservice.register(user);
                userservice.addbaseinfo();
                return new Result(true,"注册成功！！！",200,user);
            }

        }
        return new Result(true,"注册失败！！！验证码错误或验证码过期，请重新发送",400,null);
    }

    /**
     * 验证码发送接口
     * @param map
     * @return
     */
    @GetMapping("/user/sendCode")
    public Result sendCode(@RequestParam Map<String,String> map){
        //首先对数据库中是否有重复的邮箱（用户名） 进行判断
        if(userservice.findUser(map.get("uname")) == null){
            //获取当前时间
            Long time=System.currentTimeMillis();
            //初始化验证码
            verificationCode=new VerificationCode(map.get("uname"),time);
            //发送验证码
            mailService.sendMail(map.get("uname"),verificationCode.getCode());

            return new Result(true,"验证码发送成功",200,"{}");

        }
        //发送验证码失败，出现重复用户名
        return new Result(false,"发送验证码失败，用户名重复！！！",500,null);
    }

    /**
     * 修改个人信息
     * @param map
     * @return
     */
    @PostMapping("/user/chageinfo")
    public Result chageinfo(@RequestParam Map<String,String> map){
        Userinfo userinfo=new Userinfo(map);
        userservice.chageinfo(userinfo);
        return new Result(true,"修改个人信息成功",200,null);
    }

    /**
     * 登录状态下的修改密码
     * @param map
     * @return
     */
    @PostMapping("/user/chagepwd")
    public Result chagepwd(@RequestParam Map<String,String> map){
        User user=new User(map);
        int bl=userservice.chagepwd(user);
        if(bl==0){
            return new Result(false,"密码不能小于6位",500,null);
        }
        return new Result(true,"密码修改成功",400,null);
    }

    /**
     * 忘记密码？
     * @param map
     * @return
     */
    @PostMapping("/user/findpwd")
    public Result findpwd(@RequestParam Map<String,String> map) {
        String uname = map.get("uname");
        String code = map.get("code");
        //首先对数据库中是否有重复的邮箱（用户名） 进行判断
        if (userservice.findUser(map.get("uname")) != null) {
            //判断验证码是否正确
            if (verificationCode.selectCode(uname).equals(code)) {
                //判断验证码是否过期
                if (verificationCode.isvalid(uname, System.currentTimeMillis())) {
        User user=new User(map);
        int bl=userservice.chagepwd(user);
        if(bl==0){
            return new Result(false,"密码不能小于6位",500,null);
        }
            return new Result(true, "修改密码成功！！！", 200, null);
                }
            }
            return new Result(false,"验证码错误或验证码过期，请重新发送",500,null);
        }
        return new Result(false,"找不该邮箱",500,null);
    }

    /**
     * 忘记密码向邮箱发送验证码
     * @param map
     * @return
     */
    @GetMapping("/user/findpwd/sendCode")
    public Result sendCode2(@RequestParam Map<String,String> map){
            //获取当前时间
            Long time=System.currentTimeMillis();
            //初始化验证码
            verificationCode=new VerificationCode(map.get("uname"),time);
            //发送验证码
            mailService.sendMail(map.get("uname"),verificationCode.getCode());
            return new Result(true,"验证码发送成功",200,"{}");
    }

    /**
     * 修改用户权限
     * @param map
     * @return
     */
    @PostMapping("/user/admin/chageroot")
    public Result chageroot(@RequestParam Map<String,String> map){
        int userid= Integer.parseInt(map.get("userid"));
        if(userservice.returnroot(userid).equals("系统管理员")){
            User user=new User(Integer.parseInt(map.get("id")),map.get("type"));
            userservice.chageroot(user);
            return new Result(true,"更改用户权限成功",200,null);
        }
        return new Result(false,"你不是管理员！！！",500,null);
    }
}
