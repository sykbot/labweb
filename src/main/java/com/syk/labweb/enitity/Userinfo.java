package com.syk.labweb.enitity;


import lombok.Data;

import java.util.Map;

/**
 *用户的详细信息
 */
@Data
public class Userinfo {
    /**
     *用户编号
     */
    private int id;
    /**
     * 昵称
     */
    private String nickname;
    /**
     * 头像链接
     */
    private String avatar;
    /**
     *个人介绍
     */
    private String introduce;
    /**
     *主攻方向
     */
    private String field;

    public Userinfo(String nickname,String avatar){
        this.nickname=nickname;
        this.avatar=avatar;
    }

    public Userinfo(int id, String nickname, String avatar, String introduce, String field) {
        this.id = id;
        this.nickname = nickname;
        this.avatar = avatar;
        this.introduce = introduce;
        this.field = field;
    }
    public Userinfo(Map<String,String> map){
        id= Integer.parseInt(map.get("id"));
        nickname=map.get("nickname");
        avatar=map.get("avatar");
        introduce=map.get("introduce");
        field=map.get("field");
    }

    @Override
    public String toString() {
        return "User{" +
                ",id='" + id + '\'' +
                ", nickname='" + nickname + '\'' +
                ", avatar=" + avatar +
                '}';
    }

}
