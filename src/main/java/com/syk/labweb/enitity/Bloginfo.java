package com.syk.labweb.enitity;

import lombok.Data;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;


/**
 * 博客具体信息
 * @author syk
 */
@Data
public class Bloginfo {
    /**
     * 博客id
     */
    private int blogid;
    /**
     * 博主id
     */
    private int userid;
    /**
     * 博客标题
     */
    private String title;
    /**
     * 博客摘要
     */
    private String summary;
    /**
     * 博客内容
     */
    private String content;
    /**
     * 博客分类类
     */
    private String kind;
    /**
     * 博客发布时间
     */
    private String pubdate;

    public Bloginfo(int blogid, int userid, String title, String summary, String content, String kind, String pubdate) {
        this.blogid = blogid;
        this.userid=userid;
        this.title = title;
        this.summary = summary;
        this.content = content;
        this.kind = kind;
        this.pubdate = pubdate;
    }

    /**
     * 使用前端传回的map集合来给属性
     * @param map
     */

    public Bloginfo(Map<String,String> map){

        blogid=Integer.valueOf(map.get("blogid"));
        userid=Integer.valueOf(map.get("userid"));
        title=map.get("title");
        summary=map.get("summary");
        content=map.get("content");
        kind=map.get("kind");
        pubdate = String.valueOf(new Timestamp(new Date().getTime()));
    }

    @Override
    public String toString() {
        return "Bloginfo{" +
                "blogid=" + blogid +
                ", userid=" + userid +
                ", title='" + title + '\'' +
                ", summary='" + summary + '\'' +
                ", content='" + content + '\'' +
                ", kind='" + kind + '\'' +
                ", pubdate='" + pubdate + '\'' +
                '}';
    }
}
