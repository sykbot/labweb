package com.syk.labweb.enitity;

import lombok.Data;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;

@Data
public class Notice {
    /**
     * 公告id
     */
    private int id;

    /**
     *发布者id
     */
    private int userid;

    /**
     *公告内容
     */
    private String content;

    /**
     *发布时间
     */
    private String pubdate;


    public Notice(Map<String,String> map) {
        userid = Integer.parseInt(map.get("userid"));
        content = map.get("content");
        pubdate = String.valueOf(new Timestamp(new Date().getTime()));
    }

    public Notice(int id, int userid, String content, String pubdate) {
        this.id = id;
        this.userid = userid;
        this.content = content;
        this.pubdate = pubdate;
    }
}
