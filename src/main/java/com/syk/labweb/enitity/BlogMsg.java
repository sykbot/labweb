package com.syk.labweb.enitity;


import lombok.Data;

@Data
public class BlogMsg {
    /**
     * 点赞消息id
     */
    private int msgid;
    /**
     * url
     */
    private String url;
    /**
     * 用户id
     */
    private int userid;
    /**
     * 博客id
     */
    private int blogid;
    /**
     * 博客标题
     */
    private String blogName;
    /**
     * 消息内容
     */
    private String content;
    /**
     * 是否已读
     */
    private int isRead;

    public BlogMsg(int msgid, String url, int userid, int blogid, String blogName, String content, int isRead) {
        this.msgid = msgid;
        this.url = url;
        this.userid = userid;
        this.blogid = blogid;
        this.blogName = blogName;
        this.content = content;
        this.isRead = isRead;
    }

    public BlogMsg(String url, int userid, int blogid, String blogName) {
        this.url = url;
        this.userid = userid;
        this.blogid = blogid;
        this.blogName = blogName;
    }

    @Override
    public String toString() {
        return "BlogMsg{" +
                "msgid=" + msgid +
                ", url='" + url + '\'' +
                ", userid=" + userid +
                ", blogid=" + blogid +
                ", blogName='" + blogName + '\'' +
                ", content='" + content + '\'' +
                ", isRead=" + isRead +
                '}';
    }
}
