package com.syk.labweb.enitity;

import lombok.Data;

import java.util.Map;

/**
 * 用户注册信息
 * @author syk
 */

@Data
public class User {
    /**
     * 用户id(无需手动注册内部生成)
     */
    private int id;
    /**
     * 用户（邮箱）
     */
    private String uname;
    /**
     * 密码
     */
    private String pwd;
    /**
     * 生日
     */
    private String birthday;
    /**
     * 用户权限
     */
    private String type;


    public User(int id, String type) {
        this.id = id;
        this.type = type;
    }

    public User(Map<String,String> map) {

        id = Integer.valueOf(map.get("id"));
        pwd = map.get("pwd");
    }

    public User(String uname, String pwd, String type){
        this.uname=uname;
        this.pwd=pwd;
        this.type=type;
    }
    public User(String uname,String pwd,String birthday,String type){
        this.uname=uname;
        this.pwd=pwd;
        this.birthday=birthday;
        this.type=type;
    }

    public User(int id, String uname, String pwd, String birthday, String type) {
        this.id = id;
        this.uname = uname;
        this.pwd = pwd;
        this.birthday = birthday;
        this.type = type;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='"+id+'\''+
                ",uname='" + uname + '\'' +
                ", pwd='" + pwd + '\'' +
                ", birthday=" + birthday +
                ",type='" + type + '\'' +
                '}';
    }
}
