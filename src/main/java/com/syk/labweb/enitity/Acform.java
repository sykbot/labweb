package com.syk.labweb.enitity;

import lombok.Data;

import java.util.Map;

@Data
public class Acform {
    /**
     *报名id
     */
private int acid;
    /**
     *报名者id
     */
private int userid;
    /**
     *email
     */
private String email;
    /**
     *个人优势介绍
     */
private String perintro;


    public Acform(int userid, String email, String perintro) {
        this.userid = userid;
        this.email = email;
        this.perintro = perintro;
    }

    public Acform(int acid, int userid, String email, String perintro) {
        this.acid = acid;
        this.userid = userid;
        this.email = email;
        this.perintro = perintro;
    }
    public Acform(Map<String,String> map){
        acid= Integer.parseInt(map.get("acid"));
        userid= Integer.parseInt(map.get("userid"));
        email=map.get("email");
        perintro=map.get("perintro");
    }

    @Override
    public String toString() {
        return "Acform{" +
                "acid=" + acid +
                ", userid=" + userid +
                ", email='" + email + '\'' +
                ", perintro='" + perintro + '\'' +
                '}';
    }
}
