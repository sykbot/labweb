package com.syk.labweb.enitity;

import lombok.Data;

@Data
public class Like {

    /**
     * 被点赞的博客id
     */
    private int blogid;
    /**
     * 点赞者的id
     */
    private  int likedid;

    public Like(int blogid, int likedid) {
        this.blogid = blogid;
        this.likedid = likedid;
    }

    @Override
    public String toString() {
        return "Like{" +
                "blogid=" + blogid +
                ", likedid=" + likedid +
                '}';
    }
}
