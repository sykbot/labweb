package com.syk.labweb.enitity;


import lombok.Data;

import java.util.List;
import java.util.Map;


/**
 * 评论信息
 * @author syk
 */

@Data
public class Comment {
    /**
     * 博文id
     */
    private int blogid;
    /**
     * 活动id
     */
    private int acid;
    /**
     *用户编号
     */
    private int userid;
    /**
     * 昵称
     */
    private String nickname;
    /**
     * 头像链接
     */
    private String avatar;
    /**
     * 评论id
     * 此处是对博客评论的id
     * 若对评论的评论id则为上级评论的id
     */
    private int commentid;
    /**
     * 评论内容
     */
    private String content;
    /**
     *下级评论
     */
    private List<Comment> commentList;
    public  Comment(){}
    public Comment(Map<String,String> map){
        if(map.get("blogid")!=null){ blogid= Integer.parseInt(map.get("blogid"));}
        else {acid=Integer.parseInt(map.get("acid"));}
        userid = Integer.parseInt(map.get("userid"));
        nickname = map.get("nickname");
        avatar = map.get("avatar");
        content = map.get("content");
    }
    public Comment(int blogid,int userid,String nickname,String avatar,int commentid,String content){
        this.blogid=blogid;
        this.userid = userid;
        this.nickname = nickname;
        this.avatar = avatar;
        this.commentid = commentid;
        this.content = content;
    }

    public Comment(int acid, int userid, String nickname, String avatar, int commentid, String content, List<Comment> commentList) {
        this.acid = acid;
        this.userid = userid;
        this.nickname = nickname;
        this.avatar = avatar;
        this.commentid = commentid;
        this.content = content;
        this.commentList = commentList;
    }

    public Comment(int blogid, int acid, int userid, String nickname, String avatar, int commentid, String content) {
        this.blogid = blogid;
        this.acid = acid;
        this.userid = userid;
        this.nickname = nickname;
        this.avatar = avatar;
        this.commentid = commentid;
        this.content = content;
    }

    public void addcomment(List<Comment> list){
        commentList=list;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "blogid=" + blogid +
                ", acid=" + acid +
                ", userid=" + userid +
                ", nickname='" + nickname + '\'' +
                ", avatar='" + avatar + '\'' +
                ", commentid=" + commentid +
                ", content='" + content + '\'' +
                ", commentList=" + commentList +
                '}';
    }
}
