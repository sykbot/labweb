package com.syk.labweb.enitity;

import lombok.Data;

@Data
public class Collecte {
    /**
     * 收藏人id
     */
    private int collectionid;
    /**
     * 收藏博客id
     */
    private int blogid;

    @Override
    public String toString() {
        return "Collecte{" +
                "collectionid=" + collectionid +
                ", blogid=" + blogid +
                '}';
    }
}
