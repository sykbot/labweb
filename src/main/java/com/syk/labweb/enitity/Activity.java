package com.syk.labweb.enitity;


import lombok.Data;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;

@Data
public class Activity {
    /**
     * 活动id
     */
    private int id;
    /**
     * 活动标题
     */
    private String title;
    /**
     *活动概要
     */
    private String introduction;
    /**
     *发布人id
     */
    private int userid;
    /**
     *发布时间
     */
    private String pubdate;
    /**
     *报名人数余额
     */
    private int pernum;
    /**
     *活动类型方向
     */
    private String type;

    public Activity(int id, String title, String introduction, int userid, String pubdate,int pernum,String type) {
        this.id = id;
        this.title = title;
        this.introduction = introduction;
        this.userid = userid;
        this.pubdate = pubdate;
        this.pernum = pernum;
        this.type = type;
    }

    public Activity(Map<String,String> map) {
        title = map.get("title");
        introduction = map.get("introduction");
        userid = Integer.parseInt(map.get("userid"));
        pubdate =String.valueOf(new Timestamp(new Date().getTime()));
        pernum = Integer.parseInt(map.get("pernum"));
        type = map.get("type");
    }

    @Override
    public String toString() {
        return "Activity{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", introduction='" + introduction + '\'' +
                ", userid=" + userid +
                ", pubdate='" + pubdate + '\'' +
                ", pernum=" + pernum +
                ", type='" + type + '\'' +
                '}';
    }
}
