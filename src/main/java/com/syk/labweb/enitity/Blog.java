package com.syk.labweb.enitity;

import lombok.Data;

/**
 * 博文的相关信息
 * @author syk
 */
@Data
public class Blog {
    /**
     * 博文id
     */
    private int blogid;
    /**
     * 用户id
     */
    private int userid;
    /**
     * 点赞数
     */
    private int likenum;
    /**
     * 收藏数
     */
    private int collect;
    /**
     * 浏览量
     */
    private String views;
    /**
     * 发布时间
     */
    private String pubdate;
    /**
     * 博文题目
     */
    private String title;
    /**
     * 博文摘要
     */
    private String summary;

    public Blog(int blogid, int userid, int likenum, int collect, String views, String pubdate, String title, String summary) {
        this.blogid = blogid;
        this.userid = userid;
        this.likenum = likenum;
        this.collect = collect;
        this.views = views;
        this.pubdate = pubdate;
        this.title = title;
        this.summary = summary;
    }

    public Blog(int userid, String pubdate, String title, String summary) {
        this.userid = userid;
        this.pubdate = pubdate;
        this.title = title;
        this.summary = summary;
    }

    @Override
    public String toString() {
        return "Blog{" +
                "blogid=" + blogid +
                ", userid=" + userid +
                ", likenum=" + likenum +
                ", collect=" + collect +
                ", views='" + views + '\'' +
                ", pubdate='" + pubdate + '\'' +
                ", title='" + title + '\'' +
                ", summary='" + summary + '\'' +
                '}';
    }
}
