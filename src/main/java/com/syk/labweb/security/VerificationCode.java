package com.syk.labweb.security;

import java.util.Date;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

public class VerificationCode {

    //此处用于存放所有到访的验证码
    //为了防止产生环形链表这里采用了ConcurrentHashMap
    private static Map<String,String>  allcode=new ConcurrentHashMap<>(20);

    //此处存放验证码的时间
    private static Map<String, Long>  alltime=new ConcurrentHashMap<>(20);

    //当前产生的验证码
    private String code;

    //初始化验证码
    public  VerificationCode(String mail,Long time){
        code=bulidvd();
        addcode(mail,code);
        addtime(mail,time);

    }
    //得到当前的验证码
    public String getCode(){
        return code;
    }

    //获取注册邮箱验证码
    public String selectCode(String mail){
        return allcode.get(mail);
    }

    //加入这条验证码(相同的用户名只能存放一条)
    public void addcode(String mail,String code){
        if(allcode.get(mail)!=null){
            allcode.remove(mail);
        }
        allcode.put(mail, code);
    }

    //加入产生验证码时间(相同的用户名只能存放一条)
    public void addtime(String mail,Long time){
        if(alltime.get(mail)!=null){
            alltime.remove(mail);
        }
        alltime.put(mail, time);
    }

    //判别验证码是否过期(五分钟有效)
    public boolean isvalid(String mail,Long time){
        //时间差
        Long between=(time -alltime.get(mail))/60000;
        if(between>5){ return false; }
        else {return true;}
    }

    //随机生成验证码(6位)
     private String bulidvd(){return String.valueOf(new Random().nextInt(899999)+100000); }

    //清除已过期的验证码
    public void clearcode(String mail){
        allcode.remove(mail);
        alltime.remove(mail);
    }

}
