-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        8.0.23 - MySQL Community Server - GPL
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  10.2.0.5704
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 导出  表 example.sdcomment 结构
CREATE TABLE IF NOT EXISTS `sdcomment` (
  `commentid` int DEFAULT NULL COMMENT '上级评论id',
  `blogid` int DEFAULT NULL COMMENT '博客id',
  `acid` int DEFAULT NULL COMMENT '活动id',
  `userid` int NOT NULL COMMENT '评论者id',
  `nickname` char(50) NOT NULL COMMENT '评论者昵称',
  `avatar` char(50) NOT NULL COMMENT '评论者头像地址',
  `content` char(50) NOT NULL COMMENT '评论内容',
  KEY `FK_sdcomment_comment` (`commentid`),
  CONSTRAINT `FK_sdcomment_comment` FOREIGN KEY (`commentid`) REFERENCES `comment` (`commentid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- 正在导出表  example.sdcomment 的数据：~9 rows (大约)
/*!40000 ALTER TABLE `sdcomment` DISABLE KEYS */;
INSERT INTO `sdcomment` (`commentid`, `blogid`, `acid`, `userid`, `nickname`, `avatar`, `content`) VALUES
	(1, 1, NULL, 1, 'john', '地址', '这是评论内容'),
	(1, 1, NULL, 2, 'mike', '地址', '这是评论内容'),
	(1, 1, NULL, 3, 'sara', '地址', '这是评论内容'),
	(2, 1, NULL, 4, 'siri', '地址', '这是评论内容'),
	(2, 1, NULL, 5, 'jack', '地址', '这是评论内容'),
	(39, 1, NULL, 1, 'sykbot', 'C:', '你这个不对'),
	(39, 1, NULL, 1, 'sykbot', 'C:', '你这个不对'),
	(39, 1, NULL, 1, 'sykbot', 'C:', '你这个不对'),
	(39, 1, NULL, 1, 'sykbot', 'C:', '你这个不对'),
	(39, NULL, 1, 1, 'sykbot', 'C:', '你这个对'),
	(43, NULL, 1, 1, 'sykbot', 'C:', '你这个对'),
	(39, 1, NULL, 1, 'sykbot', 'C:', '你这个不对');
/*!40000 ALTER TABLE `sdcomment` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
