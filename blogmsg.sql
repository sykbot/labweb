-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        8.0.23 - MySQL Community Server - GPL
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  10.2.0.5704
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 导出  表 example.blogmsg 结构
CREATE TABLE IF NOT EXISTS `blogmsg` (
  `msgid` int NOT NULL AUTO_INCREMENT,
  `url` char(50) DEFAULT NULL,
  `userid` int DEFAULT NULL,
  `blogid` int DEFAULT NULL,
  `blogName` char(50) DEFAULT NULL,
  `content` char(50) DEFAULT NULL,
  `isRead` bit(1) DEFAULT NULL,
  PRIMARY KEY (`msgid`),
  KEY `FK_blogmsg_blog` (`blogid`),
  CONSTRAINT `FK_blogmsg_blog` FOREIGN KEY (`blogid`) REFERENCES `blog` (`blogid`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- 正在导出表  example.blogmsg 的数据：~10 rows (大约)
/*!40000 ALTER TABLE `blogmsg` DISABLE KEYS */;
INSERT INTO `blogmsg` (`msgid`, `url`, `userid`, `blogid`, `blogName`, `content`, `isRead`) VALUES
	(2, '3', 1, 1, '2', '收到一个点赞啦！！！', b'1'),
	(7, '/blog/collect', 1, 1, '如何学习java', '被其他用户收藏了！！！', b'1'),
	(17, '/comments/blog/commentcm', 1, 1, '如何开发一个实验室官网？', '被其他用户评论啦！！！', b'1'),
	(18, '/blog/like', 1, 1, '如何开发一个实验室官网？', '收到一个点赞啦！！！', b'1'),
	(19, '/blog/collect', 1, 3, '如何学习java', '被其他用户收藏了！！！', b'1'),
	(20, '/comments/blog/commentblog', 1, 1, '如何开发一个实验室官网？', '被其他用户评论啦！！！', b'0'),
	(21, '/comments/blog/commentcm', 1, 1, '如何开发一个实验室官网？', '被其他用户评论啦！！！', b'0');
/*!40000 ALTER TABLE `blogmsg` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
