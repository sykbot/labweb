-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        8.0.23 - MySQL Community Server - GPL
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  10.2.0.5704
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 导出  表 example.opform 结构
CREATE TABLE IF NOT EXISTS `opform` (
  `blogid` int NOT NULL COMMENT '博客id',
  `likedid` int DEFAULT NULL COMMENT '点赞者id',
  `collectionid` int DEFAULT NULL COMMENT '收藏者id',
  `commentid` int DEFAULT NULL COMMENT '评论者id',
  KEY `FK_opform_blog` (`blogid`),
  CONSTRAINT `FK_opform_blog` FOREIGN KEY (`blogid`) REFERENCES `blog` (`blogid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- 正在导出表  example.opform 的数据：~3 rows (大约)
/*!40000 ALTER TABLE `opform` DISABLE KEYS */;
INSERT INTO `opform` (`blogid`, `likedid`, `collectionid`, `commentid`) VALUES
	(1, NULL, NULL, 39),
	(1, NULL, NULL, 0),
	(1, NULL, NULL, 39),
	(1, NULL, NULL, 0),
	(1, NULL, NULL, 39),
	(1, 1, NULL, NULL),
	(1, NULL, NULL, 0),
	(1, NULL, NULL, 39);
/*!40000 ALTER TABLE `opform` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
