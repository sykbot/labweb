-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        8.0.23 - MySQL Community Server - GPL
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  10.2.0.5704
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 导出  表 example.comment 结构
CREATE TABLE IF NOT EXISTS `comment` (
  `commentid` int NOT NULL AUTO_INCREMENT COMMENT '评论id',
  `blogid` int DEFAULT NULL COMMENT '博客id',
  `acid` int DEFAULT NULL COMMENT '活动id',
  `userid` int DEFAULT NULL COMMENT '评论者id',
  `nickname` char(50) DEFAULT NULL COMMENT '评论者昵称',
  `avatar` char(50) DEFAULT NULL COMMENT '评论者头像地址',
  `content` char(50) DEFAULT NULL COMMENT '评论内容',
  PRIMARY KEY (`commentid`),
  KEY `FK_comment_blog` (`blogid`),
  KEY `FK_comment_activity` (`acid`),
  CONSTRAINT `FK_comment_activity` FOREIGN KEY (`acid`) REFERENCES `activity` (`id`),
  CONSTRAINT `FK_comment_blog` FOREIGN KEY (`blogid`) REFERENCES `blog` (`blogid`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- 正在导出表  example.comment 的数据：~22 rows (大约)
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
INSERT INTO `comment` (`commentid`, `blogid`, `acid`, `userid`, `nickname`, `avatar`, `content`) VALUES
	(1, 1, NULL, 1, '落落青城宝贝儿', '地址', '这是评论内容'),
	(2, 1, NULL, 2, '落落青城宝贝儿', '地址', '这是评论内容'),
	(3, 1, NULL, 3, '落落青城宝贝儿', '地址', '这是评论内容'),
	(4, 1, NULL, 4, '落落青城宝贝儿', '地址', '这是评论内容'),
	(5, 1, NULL, 5, '落落青城宝贝儿', '地址', '这是评论内容'),
	(6, 1, NULL, 6, '落落青城宝贝儿', '地址', '这是评论内容'),
	(7, 1, NULL, 7, '落落青城宝贝儿', '地址', '这是评论内容'),
	(8, 1, NULL, 8, '落落青城宝贝儿', '地址', '这是评论内容'),
	(9, 1, NULL, 9, '落落青城宝贝儿', '地址', '这是评论内容'),
	(10, 1, NULL, 10, '落落青城宝贝儿', '地址', '这是评论内容'),
	(11, 1, NULL, 11, '落落青城宝贝儿', '地址', '这是评论内容'),
	(12, 1, NULL, 12, '落落青城宝贝儿', '地址', '这是评论内容'),
	(13, 1, NULL, 13, '落落青城宝贝儿', '地址', '这是评论内容'),
	(14, 1, NULL, 14, '落落青城宝贝儿', '地址', '这是评论内容'),
	(15, 1, NULL, 15, '落落青城宝贝儿', '地址', '这是评论内容'),
	(16, 1, NULL, 16, '落落青城宝贝儿', '地址', '这是评论内容'),
	(17, 1, NULL, 17, '落落青城宝贝儿', '地址', '这是评论内容'),
	(18, 1, NULL, 18, '落落青城宝贝儿', '地址', '这是评论内容'),
	(19, 1, NULL, 19, '落落青城宝贝儿', '地址', '这是评论内容'),
	(20, 1, NULL, 20, '落落青城宝贝儿', '地址', '这是评论内容'),
	(39, 3, NULL, 4, '落落青城宝贝儿', '地址', '写的不错'),
	(40, 1, NULL, 1, 'sykbot', 'C:', '写的不错'),
	(41, 1, NULL, 1, 'sykbot', 'C:', '写的不错'),
	(42, 1, NULL, 1, 'sykbot', 'C:', '写的不错'),
	(43, NULL, 1, 1, 'sykbot', 'C:', '写的不错'),
	(44, 1, NULL, 1, 'sykbot', 'C:', '写的不错');
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
