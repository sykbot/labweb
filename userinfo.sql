-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        8.0.23 - MySQL Community Server - GPL
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  10.2.0.5704
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 导出  表 example.userinfo 结构
CREATE TABLE IF NOT EXISTS `userinfo` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `nickname` char(50) DEFAULT NULL COMMENT '昵称',
  `avatar` char(50) DEFAULT NULL COMMENT '头像地址',
  `introduce` text COMMENT '个人介绍',
  `field` char(50) DEFAULT NULL COMMENT '技术方向',
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_userinfo_suser` FOREIGN KEY (`id`) REFERENCES `suser` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- 正在导出表  example.userinfo 的数据：~4 rows (大约)
/*!40000 ALTER TABLE `userinfo` DISABLE KEYS */;
INSERT INTO `userinfo` (`id`, `nickname`, `avatar`, `introduce`, `field`) VALUES
	(1, 'sykbot', 'C:', '我是sykbot', 'java'),
	(2, '落落青城宝贝儿', '地址', NULL, NULL),
	(4, '落落青城宝贝儿', '地址', NULL, NULL),
	(10, '落落青城宝贝儿', '地址', NULL, NULL),
	(11, '用户##', '地址##', NULL, NULL);
/*!40000 ALTER TABLE `userinfo` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
