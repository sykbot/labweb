-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        8.0.23 - MySQL Community Server - GPL
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  10.2.0.5704
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 导出  表 example.activity 结构
CREATE TABLE IF NOT EXISTS `activity` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '活动id',
  `title` char(50) DEFAULT NULL COMMENT '活动标题',
  `introduction` varchar(50) DEFAULT NULL COMMENT '活动介绍',
  `userid` int DEFAULT NULL COMMENT '发布者id',
  `pubdate` varchar(50) DEFAULT NULL COMMENT '发布时间',
  `pernum` int DEFAULT NULL COMMENT '报名剩余人数',
  `type` char(50) DEFAULT NULL COMMENT '比赛方向',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- 正在导出表  example.activity 的数据：~3 rows (大约)
/*!40000 ALTER TABLE `activity` DISABLE KEYS */;
INSERT INTO `activity` (`id`, `title`, `introduction`, `userid`, `pubdate`, `pernum`, `type`) VALUES
	(1, '创新大赛', '系统创新', 4, '2021-08-20', 40, 'java'),
	(2, '互联网+', '互联网大赛', 1, '2021-08-20', 20, 'all'),
	(4, '服务外包', '企业外包项目', 4, '2021-08-21 19:13:53.029', 60, 'all');
/*!40000 ALTER TABLE `activity` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
