-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        8.0.23 - MySQL Community Server - GPL
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  10.2.0.5704
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 导出  表 example.blog 结构
CREATE TABLE IF NOT EXISTS `blog` (
  `blogid` int NOT NULL AUTO_INCREMENT COMMENT '博客id',
  `userid` int DEFAULT NULL COMMENT '发布者id',
  `likenum` int DEFAULT '0' COMMENT '点赞数',
  `collect` int DEFAULT '0' COMMENT '收藏数',
  `views` char(50) DEFAULT '0' COMMENT '阅览数',
  `title` char(50) DEFAULT NULL COMMENT '博客标题',
  `summary` char(50) DEFAULT NULL COMMENT '博客概要',
  `pubdate` char(50) DEFAULT NULL COMMENT '发布时间',
  PRIMARY KEY (`blogid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- 正在导出表  example.blog 的数据：~2 rows (大约)
/*!40000 ALTER TABLE `blog` DISABLE KEYS */;
INSERT INTO `blog` (`blogid`, `userid`, `likenum`, `collect`, `views`, `title`, `summary`, `pubdate`) VALUES
	(1, 1, 1, 520, '1314', '如何开发一个实验室官网?', '本文将简述如何开发一个实验室官网', '2021/7/30 10:14'),
	(3, 1, 0, 0, '0', '刻苦学习', '2021-08-13 20:19:42.107', '如何学习java'),
	(4, 11, 0, 0, '0', '大作业概要', '2021-08-21 18:56:53.042', ' 21届软件创新实验室暑假集训后端赛道大作业'),
	(5, 11, 0, 0, '0', '大作业概要', '2021-08-21 18:58:45.846', ' 21届软件创新实验室暑假集训后端赛道大作业');
/*!40000 ALTER TABLE `blog` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
